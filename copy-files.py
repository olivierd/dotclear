#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# SPDX-FileCopyrightText: NONE
# SPDX-License-Identifier: Unlicense
#

'''Get the latest modified files (when an update is available).'''

import argparse
import os
import pathlib
import sys


def dc_dir_exists(path, dirname):
    res = False

    for i in pathlib.Path(path).iterdir():
        if i.is_dir():
            if pathlib.PurePath(i.name).match('dotclear-*'):
                res = True
                dirname = '{0}'.format(i)

    return res, dirname

def dc_make_directories(dst, parents=False):
    if not pathlib.Path(dst).exists():
        if parents:
            os.makedirs(dst)
        else:
            os.mkdir(dst)

def dc_make_copy(dst, fd, previous_dir):
    pathname = None

    dc_make_directories(dst)

    line = fd.readline()
    while line:
        pathname = os.path.dirname(line.replace(os.linesep, ''))
        filename = os.path.basename(line.replace(os.linesep, ''))
        if pathname:
            new_dir = os.path.join(dst, pathname)
            dc_make_directories(new_dir, parents=True)

            new_file = os.path.join(new_dir, filename)
            os.replace(os.path.join(previous_dir, pathname, filename),
                       new_file)
        else:
            os.replace(os.path.join(previous_dir, filename),
                       os.path.join(dst, filename))

        line = fd.readline()
    
def main(opts):
    dc_name = ''
    p = pathlib.Path(''.join(opts.dir)).resolve()
    file_listing = pathlib.Path(opts.file).name

    if p.joinpath(file_listing).exists():
        res, dc_name = dc_dir_exists(p, dc_name)
        if res:
            with open(os.path.join(p, file_listing), 'r') as f:
                # We need to cast the 'p' variable because it is
                # a pathlib.PosixPath instance
                dc_make_copy(os.path.join('{0}'.format(p), 'dotclear'), f,
                             dc_name)
    else:
        print('Error: no such file or directory')
        sys.exit(-1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('dir', nargs=1, help='root directory')
    parser.add_argument('-f', '--file', default='updated.txt',
                        help='listing (updated.txt)')
    main(parser.parse_args())
