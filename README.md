<!--
SPDX-FileCopyrightText: NONE

SPDX-License-Identifier: Unlicense
-->

Scripts to easily upgrade instance of [Dotclear](https://fr.dotclear.org/).

### Requirements

- curl
- patch
- python3
- make

### How to use it?

1. Change **LEGACY_VERSION** and **CURRENT_VERSION** in `update.sh`.
2. Run `make`

Then copy contents of directory `~/tmp/dotclear/`.

3. Don't forget to remove files listed in `deleted.txt`.    

A [French version](https://avignu.com/dotclear/index.php?post/2018/01/21/Explication-sur-comment-mettre-%C3%A0-jour-Dotclear-sur-notre-serveur) is available too.
