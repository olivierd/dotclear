# SPDX-FileCopyrightText: NONE
# SPDX-License-Identifier: Unlicense

# Get the latest modified files when an update of Dotclear is available.
#
# Before to run this Makefile, check out the LEGACY_VERSION and
# UPDATE_VERSION variables in 'update.sh' script.
#
# After, copy content of ~/tmp/dotclear/ to remote host

PYTHON_CMD=	/usr/bin/python3

all: copy

copy: update
	@$(PYTHON_CMD) copy-files.py ~/tmp

update:
	sh update.sh

clean:
	@$(RM) -Rf ~/tmp

.PHONY: update copy
